
#!/bin/bash

# Check for clang (LLVM)
echo "Checking for LLVM (clang)..."
if ! command -v clang &> /dev/null
then
    echo "LLVM (clang) could not be found. Please install LLVM manually."
    exit 1
fi

# Check for Godot engine
echo "Checking for Godot engine..."
if ! command -v godot &> /dev/null
then
    echo "Godot engine could not be found. Please install Godot manually."
    exit 1
fi

# Check for Rust
echo "Checking for Rust..."
if ! command -v rustc &> /dev/null
then
    echo "Rust is not installed. Installing Rust via rustup..."
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
    source "$HOME/.cargo/env"
else
    echo "Rust is already installed."
fi

# Check for just
echo "Checking for just..."
if ! cargo install --list | grep -q 'just v'; then
    echo "just is not installed. Installing just via cargo..."
    cargo install just
else
    echo "just is already installed."
fi

# Check for watchexec
echo "Checking for watchexec-cli..."
if ! cargo install --list | grep -q 'watchexec-cli v'; then
    echo "watchexec-cli is not installed. Installing watchexec-cli via cargo..."
    cargo install watchexec-cli
else
    echo "watchexec-cli is already installed."
fi

# Navigate to rust subfolder and run just --list
echo "Navigating to rust subfolder and running 'just --list'..."
cd rust && just --list
