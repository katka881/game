use godot::prelude::*;

pub mod cracktangle;
pub mod entities;

struct MyExtension;

#[gdextension]
unsafe impl ExtensionLibrary for MyExtension {}
