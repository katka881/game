use godot::engine::Node2D;
use godot::prelude::*;
use nanorand::{Rng, WyRand};

#[derive(GodotClass)]
#[class(base=Node2D)]
struct Screamer {
	rng: WyRand,
}

#[godot_api]
impl INode2D for Screamer {
	fn init(_base: Base<Node2D>) -> Self {
		Self { rng: WyRand::new() }
	}

	fn process(&mut self, delta: f64) {
		if (delta as usize % 200 == 0) && self.rng.generate::<bool>() {
			let mut message = String::new();

			for _ in 0..(self.rng.generate::<u8>() % 32) {
				if self.rng.generate::<bool>() {
					message.push('A');
				} else {
					message.push('a');
				}
			}

			godot_print!("absolute screamer: {message}");
		}
	}
}
