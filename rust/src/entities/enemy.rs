use godot::prelude::*;
:q

#[derive(GodotClass)]
#[class(base=AnimatedSprite3D)]
struct Enemy {
	// the AI state
	state: State,
	last_rand: u8,
	base: Base<AnimatedSprite3D>,
}

enum State {
	Anticipate = 0,
	Attack,
	Circle,
}

#[godot_api]
impl IAnimatedSprite3D for Enemy {

	fn init(base: Base<AnimatedSprite3D>) -> Self {
		Self {
			state: Circle,
			last_rand: 2,
			base,
		}
	}

	fn physics_process(&mut self, delta: f64) {
		todo!();
	}
}
